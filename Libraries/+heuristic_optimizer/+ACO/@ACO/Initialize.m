function obj = Initialize(obj, initialPosition)
% Initialize: Initialize the population of ACO.
%
% Input:
% obj: ACO class object
% initialPosition: Optional custom initialization. va definito come
% rappresentare la posizione delle formiche. In teoria dovrebbe essere
% sufficiente indicare l'etichetta di nodo (1, 2, 3, ..., etc)
%
% Output:
% obj: ACO class object

% Import library
import heuristic_optimizer.ACO.*

% Create the population
for p = 1:obj.generalOptions.numIndividuals
	% Call the ParticleClass constructor
	obj.population(p,1) = Ant( ... vanno definiti i parametri da passare al costruttore della classe Ant
                                );   
end
	
% Default Initialization
if ~exist('initialPosition', 'var')
    for p = 1:obj.generalOptions.numIndividuals
        obj.population(p) = obj.population(p).Initialize(); 
    end
	
% Custom Initialization
else
    for p = 1:obj.generalOptions.numIndividuals
        % Get the p-th position
        obj.population(p).node = initialPosition(:, p); ... sto supponendo che la posizione della formica sia data dalla proprietÓ node
    end
end

end