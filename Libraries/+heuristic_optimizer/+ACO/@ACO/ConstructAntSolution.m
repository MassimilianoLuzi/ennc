function obj = ConstructAntSolution(obj)
% ConstructAntSolution: Contruct the movements of ants
%
% Input:
% obj: ACO class object
%
% Output:
% obj: ACO class object

% Import Library
import heuristic_optimizer.ACO.*

% Update the population
for p=1:obj.generalOptions.numIndividuals
    obj.localPopulation(p) = obj.localPopulation(p).MoveAnt(obj.Fitness, obj.fitnessOptions); 
end
    
end