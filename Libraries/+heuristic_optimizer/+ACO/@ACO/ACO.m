classdef ACO
% Main class of the Ant Colony Optimization algorithm
    properties
        % Ant Population
        population;
        Adj; 
        tau; 
        
        % Fitness
        Fitness;
        fitnessOptions;
        
        % Global bests
        bestAnt;
        old_bestAnt;
                
        % General Options
        generalOptions;
        
        % PSO Options
        ACOOptions;
        
        % Verbose
        verboseFlag;
    end
    
    methods
        % Constructor
        function obj = ACO(p_Fitness, p_Adj, varargin)
			% ACO: Constructor of the ACO optimizer
			%
			% Input:
			% p_Fitness: Handle to the fitness function
			% p_adjacency: adjacency matrix of the graph to perform
			% optimization
			% varargin: Supplementary options
			%
			% Output:
			% obj: ACO class object
			
			% Import library
            import heuristic_optimizer.ACO.*
            
			% Handle for the minimal number of required arguments
            if nargin <2
                error('The constructor must receive at leas two arguments: 1. The handle to the fitness function and 2. the number of parameters to optimize');
            end
            
            % Default algorithm parameters
            default_generalOptions = struct(...
                'parallelFlag',             false,      				... flag for the parallel evaluation
                'numIndividuals',           50,         				... num individuals of the pso
                'maxIteration',             100,         				... max num of iteration
                'consecutiveTrueTh',        50,         				... threshold for the stop condition
                'stopThreshold',            1e-5,       				... threshold on the difference between old and new gBest
                'fitnessStopThreshold',     1e-5        				... threshold on the difference between old best fitness and new best fitness
                );
            
            % Default ACO parameters
            % Da capire quali sono i metaparametri di aco. Ad esempio q0 
            default_ACOOptions = struct(      ...  
                'alpha',    1,                          ...
                'beta',     4,                          ...
                'tau_0',    0.0001*ones(size(Adj,1)),   ...
                'rho',      0.15,                       ...   
                'phi',      0.15,                        ...
                'el',       0.97                        ...
                );

            % Define the input parser - serve per generalizzare gli input
            p = inputParser;
            p.addRequired('p_Fitness');
            p.addRequired('p_adjacency');
            p.addParameter('generalOptions', default_generalOptions);
            p.addParameter('ACOOptions', default_ACOOptions);
            p.addParameter('fitnessOptions', []);
            p.addParameter('Verbose', true);
           
            % Parse arguments
            p.parse(p_Fitness, p_Adj, varargin{:});
            
            % Fitness Options
            obj.Fitness = p_Fitness;
            obj.fitnessOptions = p.Results.fitnessOptions;
            
            % Graph information
            obj.Adj = p_Adj;
            
            % General Options
            obj.generalOptions = p.Results.generalOptions;
            
			% Handle for undefined struct fields
            if ~isfield(obj.generalOptions, 'parallelFlag')
                obj.generalOptions.parallelFlag = false;
            end
            
            if ~isfield(obj.generalOptions, 'numIndividuals')
                obj.generalOptions.numIndividuals = 50;
            end
            
            if ~isfield(obj.generalOptions, 'maxIteration')
                obj.generalOptions.maxIteration = 500;
            end
            
            if ~isfield(obj.generalOptions, 'consecutiveTrueTh')
                obj.generalOptions.consecutiveTrueTh = 20;
            end
            
            if ~isfield(obj.generalOptions, 'stopThreshold')
                obj.generalOptions.stopThreshold = 1e-6;
            end
            
            if ~isfield(obj.generalOptions, 'fitnessStopThreshold')
                obj.generalOptions.fitnessStopThreshold = 1e-6;
            end
            
            % ACO Options
            obj.ACOOptions = p.Results.ACOOptions;
            
			% Handle for undefined struct fields
            if ~isfield(obj.ACOOptions, 'alpha')
                obj.ACOOptions.alpha = 1;
            end
            
            if ~isfield(obj.ACOOptions, 'beta')
                obj.ACOOptions.beta = 4;
            end
            
            if ~isfield(obj.ACOOptions, 'tau_0')
                obj.ACOOptions.tau_0 = 500;
            end
            
            if ~isfield(obj.ACOOptions, 'rho')
                obj.ACOOptions.rho = 0.15;
            end
            
            if ~isfield(obj.ACOOptions, 'phi')
                obj.ACOOptions.phi = 0.15;
            end
            
            if ~isfield(obj.ACOOptions, 'el')
                obj.ACOOptions.el = 0.97;
            end
            
            % Verbose
            obj.verboseFlag = p.Results.Verbose;
            
            % Define swarm
            obj.population = Ant.empty;
                    
            % Define gBest and old_gBest
            obj.bestAnt = struct('node', [], 'fitness', inf);    ... ho messo solo due campi che dovrebbero esserci. Devono essere definiti durante lo sviluppo
            obj.old_bestAnt = struct('node', [], 'fitness', inf);
        end
    end
end