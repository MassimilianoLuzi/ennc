function stopFlag = StopCondition(obj)
% StopCondition: Verify Stop Condition
%
% Input:
% obj: ACO class object
%
% Output:
% stopFlag: Binary flag. 
%			stopFlag=1 if the fitness value does not change significantly 
%                      from the previous iteration/generation.
%			stopFlag=0 otherwise.

% Import Library
import heuristic_optimizer.ACO.*

% Initialize flag
stopFlag = false;

% Initialize vector containing the distance between old and new fitness
deltaFitness = zeros(obj.generalOptions.numBests,1);


% Evaluate the absolute difference among the previous and the actual fitness values
deltaFitness(b) = abs(obj.bestAnt.fitness - obj.old_bestAnt.fitness); 

% Check if all the stop condition are verified for all the gBest
if (deltaFitness < obj.generalOptions.fitnessStopThreshold)
	stopFlag = true;
end

end