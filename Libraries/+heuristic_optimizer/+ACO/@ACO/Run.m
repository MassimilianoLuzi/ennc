function [bestPath, bestFitness, history, convergenceTime, convergenceIter] = Run(a,b,c,d)
% Run: Run the ACO algorithm. 
% The algorithm stop if:
%	1. Both all the the fitness value do not change significantly for consecutiveTrueTh number of iterations/generations.
%	2.The maximum number of iterations/generations is reached.
%
% Input:
% obj: ACO class object
%
% Output:
% obj: ACO class object
% bestPath: best path. It represents the solution to the optimization problem
% bestFitness: fitness related to the best parameters set
% history: Evolution of the fitness over the iterations/generations
% convergenceTime: Time required for finding a solution
% convergenceIter: Iteration/generation in which the algorithms stopped

% Import Library
import heuristic_optimizer.ACO.*

% Start wall clock
timer = tic;

% Initialize consecutiveTrue fr stop condition
consecutiveTrue = 0;

% Initialize fitness history
history = zeros(obj.generalOptions.maxIteration, 1);

% Optimization core
for iter = 1:obj.generalOptions.maxIteration
    % Start iteration timer
    iterTime = tic;
    
    % Store best fitness
    history(iter) = min([obj.bestAnt.fitness]);
    
    % Verify Stop Condition
    if obj.StopCondition()
        % Increment consecutiveTrue counter if the StopCondition is verified
        consecutiveTrue = consecutiveTrue + 1;
    else
        % reset counter
        consecutiveTrue = 0;
    end
    
    % If the counter reached the threshold stop the algorithm
    if consecutiveTrue > obj.generalOptions.consecutiveTrueTh
        break;
    else
        % Save gBest for the evaluation of the stop condition
        obj.old_bestAnt = obj.bestAnt;
    end
    
    % Construct Ant Solution
    obj = obj.ConstructAntSolution();
    
    % Update Pheromone
    obj = obj.UpdatePheromone();
    
    % Update Best Ant
    obj = obj.UpdateBestAnt();
    
    % Print log
    if obj.verboseFlag
        iterTime = toc(iterTime);
        fprintf('Iter: %d/%d \t Best Fitness: %d \t Stop Condition: %d/%d \t Iter Time: %f \n', ...
            iter, obj.generalOptions.maxIteration, obj.bestAnt.fitness, consecutiveTrue, obj.generalOptions.consecutiveTrueTh, iterTime);
    end
end

% End of optimization

% Save best solution
bestPath = obj.bestAnt.path;
bestFitness = obj.bestAnt.fitness;

% clear history in case of premature convergence
history(iter-1:end) = [];

% Save convergence information
convergenceIter = iter;
convergenceTime = toc(timer);

end