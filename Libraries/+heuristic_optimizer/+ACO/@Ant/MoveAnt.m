function obj = MoveAnt(obj, p_Fitness, p_fitnessOptions)
% MoveAnt: Perform a ACO step
%
% Input:
% obj: Ant class object
% p_Fitness: Handle to the fitness function
% p_fitnessOptions: struct containing the further options and variables required for evaluating the fitness.
%
% Output:
% obj: Ant class object 

numNodes = size(obj.Adj,1);

%% Initialize initial Node
path = randi(numNodes, 1);

temp_mh = mh;

for j=1:nNodes-1             % ripeti per tutte le citt�
    c=tour(i,j);    % la formica k � sita nella citt� i e si muove verso la citt� j,
    % cio� si muove verso il nodo successivo
    temp_mh(:,c) = 0; % annulla la probabilit� di ritornare su questo nodo
    temp=(tau(c,:).^beta).*(temp_mh(c,:).^alpha); % decrizione della fx probabilistica secondo il criterio AS
    if sum(temp)~=0
        s=(sum(temp));      % fai la sommatoria
        path=(1/s).*temp;      % fai il rapporto per ottenere la pk
        seed = RouletteWheelSelection(path); % seleziona la citt� successiva tramite la roulette wheel
        tour(i,j+1)=seed;       % dove j+1 � la citt� successiva
    else
        break;
    end
end

if tour(i,nNodes) ~= 0
    if (Adj(tour(i,nNodes),tour(i,1))~=0)
        tour(i,end)=tour(i,1);
    end
end


end 