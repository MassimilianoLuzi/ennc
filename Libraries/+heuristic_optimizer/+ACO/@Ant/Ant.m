
classdef Ant < matlab.mixin.Copyable
% Main class defining the ant object inside the ACO algorithm
    
	properties
		% Individual properties
        path;
        Adj;
        fitness;
        	
		% ACO parameters
        alpha;
        beta;
        rho;
        phi;
        el;
    end
    
    methods
        function obj = Ant(p_Adj, p_generalOptions, p_ACOOptions)
			% Ant: Constructor of the Ant object
			%
			% Input:
			% p_adjacency: adjacency matrix of the graph
			% p_generalOptions: General options of the ACO algorithm
			% p_ACOOptions: metaparameters of ACO
			%
			% Output:
			% obj: Ant class object
			
			% Store adjacency matrix
            obj.Adj = p_Adj;
            
			% Store ACO parameters
            obj.alpha = p_ACOOptions.alpha;
            obj.beta = p_ACOOptions.beta;
            obj.rho = p_ACOOptions.rho;
            obj.phi = p_ACOOptions.phi;
            obj.el = p_ACOOptions.el;        
        end
    end
end