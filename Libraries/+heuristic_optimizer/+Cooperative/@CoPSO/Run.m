% Run
function [obj, bestParticle, bestFitness, history, convergenceTime, convergenceIter] = Run(obj)
    import heuristic_optimizer.PSO.*
    
    % Start global timer
    timer = tic;

    % Initialize consecutiveTrue fr stop condition
    consecutiveTrue = 0;
    
    % Initialize fitness history
    history = zeros(obj.generalOptions.maxIteration, 1);

    % Optimization core
    for iter = 1:obj.generalOptions.maxIteration
        % Start iteration timer
        iterTime = tic;

        % Store best fitness
        history(iter) = min([obj.gBest.fitness]);
        
        % Verify Stop Condition
        if obj.StopCondition()
            consecutiveTrue = consecutiveTrue + 1;
        else
            consecutiveTrue = 0;
        end

        if consecutiveTrue > obj.generalOptions.consecutiveTrueTh
            break;
        else
            % Save gBest
            obj.old_gBest = obj.gBest;
        end

        % Exchange particles from PSO to CPSO
        obj = ExchangeParticles(obj, 'pso2cpso');

        % Update CPSO Swarm
        if strcmp(obj.generalOptions.coopMode, 'series')
            obj = obj.UpdateCPSOSwarmsSeries();
        else
            obj = obj.UpdateCPSOSwarmsParallel();
        end

        % Exchange particles from CPSO to PSO
        obj = ExchangeParticles(obj, 'cpso2pso');

        % Update PSO Swarm
        obj = obj.UpdatePSOSwarm();

        obj = obj.UpdateGBest();
        
        % Plot swarm
        if ~isempty(obj.PlotFunction)
            fig = figure(1);
            obj.PlotFunction(fig, obj.cpso_swarms, obj.pso_swarm, obj.gBest, iter, obj.Fitness, obj.fitnessOptions);
        end

        % Print results
        iterTime = toc(iterTime);
        [~, bestGBestIndex] = min([obj.gBest.fitness]);
        fprintf('Iter: %d/%d \t Best Fitness: %d \t Stop Condition: %d/%d \t Iter Time: %f \n', ...
            iter, obj.generalOptions.maxIteration, obj.gBest(bestGBestIndex).fitness, consecutiveTrue, obj.generalOptions.consecutiveTrueTh, iterTime);
    end

    % End of optimization

    % Save gBest
    [~, bestGBestIndex] = min([obj.gBest.fitness]);
    bestParticle = obj.gBest(bestGBestIndex).position;
    bestFitness = obj.gBest(bestGBestIndex).fitness;

    % clear history
    history(iter:end) = [];
    
    % Save convergence information
    convergenceIter = iter;
    convergenceTime = toc(timer);
end