function obj = UpdateCPSOSwarmsSeries(obj)
    import heuristic_optimizer.PSO.*
    
% Update the swarm
for s=1:obj.numSwarms
    for subIter = 1:obj.generalOptions.numSubIter(s)
        obj.cpso_swarms(s).fitnessOptions.mode = 'cpso';
        obj.cpso_swarms(s).fitnessOptions.gBest = obj.gBest;
        obj.cpso_swarms(s).fitnessOptions.currentSwarm = s;

        % Update swarm
        obj.cpso_swarms(s) = obj.cpso_swarms(s).UpdateSwarm(); 

        % Update gBests
        obj.cpso_swarms(s) = obj.cpso_swarms(s).UpdateGBest();

        % Generate new particles with genetic operators
        if ~isempty(obj.cpso_swarms(s).GAOptions)
            obj.cpso_swarms(s) = obj.cpso_swarms(s).GeneticHybridization();
        end

        % Update gBest
        if obj.cpso_swarms(s).gBest.fitness < obj.gBest.fitness
            obj.gBest.position{s} = obj.cpso_swarms(s).gBest.position;
            obj.gBest.fitness = obj.cpso_swarms(s).gBest.fitness;
        end
    end
end  
end