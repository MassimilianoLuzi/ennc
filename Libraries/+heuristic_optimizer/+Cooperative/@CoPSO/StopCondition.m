% Stop Condition
function stopFlag = StopCondition(obj)
    import heuristic_optimizer.PSO.*
    
    % Initialize flag
    stopFlag = false;

    % Initialize vector containing the distance between old and new
    % gBests
    deltaBest = zeros(obj.numSwarms,1);

    % evaluate particle and fitness variations for each gBest
    for s=1:obj.numSwarms
        deltaBest(s) = immse(obj.old_gBest.position{s}, obj.gBest.position{s});
    end

    deltaFitness = abs(obj.gBest.fitness - obj.old_gBest.fitness); 
    % Check if all the stop condition are verified for each gBest
    if (prod(deltaBest < obj.generalOptions.stopThreshold) || deltaFitness < obj.generalOptions.fitnessStopThreshold)
        stopFlag = true;
    end
end