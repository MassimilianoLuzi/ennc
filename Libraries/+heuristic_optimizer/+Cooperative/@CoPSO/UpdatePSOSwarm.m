% Update PSOSwarm
function obj = UpdatePSOSwarm(obj)
    import heuristic_optimizer.PSO.*
    
    % Update Swarm
    obj.pso_swarm = obj.pso_swarm.UpdateSwarm(); 

    % Update gBests
    obj.pso_swarm = obj.pso_swarm.UpdateGBest();

    % Generate new particles with genetic operators
    if ~isempty(obj.pso_swarm.GAOptions)
        obj.pso_swarm = obj.pso_swarm.GeneticHybridization();
    end
end