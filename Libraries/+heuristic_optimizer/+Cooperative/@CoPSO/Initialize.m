 % Initialization
function obj = Initialize(obj, cpso_initialSwarm, pso_initialSwarm)
    import heuristic_optimizer.PSO.*
    
    %% Initialize PSO swarm
    pso_numParameters = sum(obj.numParameters);
    pso_generalOptions = struct(...
                'parallelFlag',             obj.generalOptions.parallelFlag,      ... flag for the parallel evaluation
                'adaptiveFlag',             obj.generalOptions.adaptiveFlag,      ... flag for the adaptive velocity update
                'numIndividuals',           obj.generalOptions.pso_numIndividuals,         ... num individuals of the pso
                'initializationMode',       obj.generalOptions.initializationMode,	... type of initialization (zero, uniform, normal)
                'initializationRange',      obj.generalOptions.initializationRange,	... range of the initialization
                'numBests',                 obj.generalOptions.numBests,          ... Number of subswarms
                'killRatio',                obj.generalOptions.killRatio,       ... Ratio for kill the subswarm in case of few individuals
                'maxDelta',                 obj.generalOptions.maxDelta,        ... max variation allowed for guaranteed convergence pso
                'maxVelocity',              obj.generalOptions.maxVelocity,        ... max variation allowed for velocity
                'maxIteration',             obj.generalOptions.maxIteration,         ... max num of iteration
                'consecutiveTrueTh',        obj.generalOptions.consecutiveTrueTh,         ... threshold for the stop condition
                'stopThreshold',            obj.generalOptions.stopThreshold,       ... threhsold on the difference between old and new gBest
                'fitnessStopThreshold',     obj.generalOptions.fitnessStopThreshold        ... threshold on the difference between old best fitness and new best fitness
                );
    obj.fitnessOptions.mode = 'pso';
    obj.pso_swarm = HGPSOClass(obj.Fitness, pso_numParameters, ...
        'generalOptions', pso_generalOptions, ...
        'fitnessOptions', obj.fitnessOptions, ...
        'PSOOptions', obj.PSOOptions,         ...
        'GAOptions', obj.GAOptions);
    if ~exist('pso_initialSwarm', 'var')
        obj.pso_swarm = obj.pso_swarm.Initialize();
    else
        obj.pso_swarm = obj.pso_swarm.Initialize(pso_initialSwarm);
    end

    %% Initilize gBests vectors at random
    obj.gBest.position = cell(obj.numSwarms, 1);
    obj.old_gBest.position = cell(obj.numSwarms, 1);
    for s=1:obj.numSwarms
        obj.gBest.position{s} = rand(obj.numParameters(s), 1);
        obj.old_gBest.position{s} = rand(obj.numParameters(s), 1);
    end
    % set the mode of fitness evaluation
    obj.fitnessOptions.mode = 'gBest';
    % Evaluate fitness for the gBests particles
    obj.gBest.fitness = obj.Fitness(obj.gBest.position, obj.fitnessOptions);
    obj.old_gBest.fitness = obj.Fitness(obj.old_gBest.position, obj.fitnessOptions);

    %% Initialize CPSO swarms
    % set local parameters for parfor
    local_psoGeneralOptions = obj.generalOptions;
    local_psoGeneralOptions.numBests = 1;

    obj.fitnessOptions.mode = 'cpso';
    obj.fitnessOptions.gBest = obj.gBest;
    % Instantiate one HGPSO per each swarm of the CPSO
    for s = 1:obj.numSwarms
        local_psoGeneralOptions.numIndividuals = obj.generalOptions.cpso_numIndividuals(s);
        obj.fitnessOptions.currentSwarm = s;
        obj.cpso_swarms(s,1) = HGPSOClass(obj.Fitness, obj.numParameters(s), ...
            'generalOptions', local_psoGeneralOptions, ...
            'fitnessOptions', obj.fitnessOptions, ...
            'PSOOptions',     obj.PSOOptions,     ...
            'GAOptions',      obj.GAOptions);
    end
    % Initialize each swarm
    local_cpso_swarms = obj.cpso_swarms;
    for s = 1:obj.numSwarms
        if ~exist('cpso_initialSwarm', 'var') 
            local_cpso_swarms(s) = local_cpso_swarms(s).Initialize();
        else
            local_cpso_swarms(s) = local_cpso_swarms(s).Initialize(cpso_initialSwarm{s});
        end
    end
    obj.cpso_swarms = local_cpso_swarms;
end