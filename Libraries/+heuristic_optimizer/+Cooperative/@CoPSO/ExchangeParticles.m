% Update CPSOSwarm
function obj = ExchangeParticles(obj, mode)
    import heuristic_optimizer.PSO.*

    % Sort cpso swarms
    for s=1:obj.numSwarms
        [~, sortingIndices] = sort([obj.cpso_swarms(s).swarm.fitness]);
        obj.cpso_swarms(s).swarm = obj.cpso_swarms(s).swarm(sortingIndices);
    end

    % Sort pso swarm
    [~, sortingIndices] = sort([obj.pso_swarm.swarm.fitness]);
    obj.pso_swarm.swarm = obj.pso_swarm.swarm(sortingIndices);

    switch mode
        case 'cpso2pso'
            numExchange = round(obj.generalOptions.exchangeRate*obj.generalOptions.pso_numIndividuals);
            substitutionIndex = round(obj.generalOptions.pso_numIndividuals/2) + ...
                    randperm(round(obj.generalOptions.pso_numIndividuals/2), numExchange);

            % Apply the exchange
            for n = 1 : numExchange
                if n==1
                    % Copy the global best of the cpso
                    obj.pso_swarm.swarm(substitutionIndex(n)).position = cell2mat(obj.gBest.position);
                    %obj.pso_swarm.swarm(substitutionIndex(n)).velocity = zeros(sum(obj.numParameters),1);
                    obj.pso_swarm.swarm(substitutionIndex(n)).fitness = obj.gBest.fitness;
                    obj.pso_swarm.swarm(substitutionIndex(n)).pBest= cell2mat(obj.gBest.position);
                    obj.pso_swarm.swarm(substitutionIndex(n)).pBestFitness = obj.gBest.fitness;
                    %obj.pso_swarm.swarm(substitutionIndex(n)).gBest = cell2mat(obj.gBest.position);
                else
                    local_particle = obj.gBest.position;
                    local_fitness = inf;
                    local_velocity = obj.gBest.position;
                    local_pBest = obj.gBest.position;
                    local_pBestFitness = inf;
                    local_gBest = obj.gBest.position;
                    for s = 1:obj.numSwarms             

                        % Select the particles from the CPSO swarm
                        % with a K tournament algorithm
                        K = round(obj.generalOptions.kRateExchange*obj.generalOptions.cpso_numIndividuals(s));
                        selectedIndex = min(randperm(obj.generalOptions.cpso_numIndividuals(s), K));

                        local_particle{s} = obj.cpso_swarms(s).swarm(selectedIndex).position;
                        local_fitness = obj.cpso_swarms(s).swarm(selectedIndex).fitness;
                        local_velocity{s} = obj.cpso_swarms(s).swarm(selectedIndex).velocity;
                        local_pBest{s} = obj.cpso_swarms(s).swarm(selectedIndex).pBest;
                        local_pBestFitness = min(local_pBestFitness, obj.cpso_swarms(s).swarm(selectedIndex).pBestFitness);
                        %local_gBest{s} = obj.cpso_swarms(s).swarm(selectedIndex).gBest;
                    end
                    obj.pso_swarm.swarm(substitutionIndex(n)).position = cell2mat(local_particle);
                    obj.pso_swarm.swarm(substitutionIndex(n)).fitness = local_fitness;
                    obj.pso_swarm.swarm(substitutionIndex(n)).velocity = cell2mat(local_velocity);
                    obj.pso_swarm.swarm(substitutionIndex(n)).pBest= cell2mat(local_pBest);
                    obj.pso_swarm.swarm(substitutionIndex(n)).pBestFitness = local_pBestFitness;
                    %obj.pso_swarm.swarm(substitutionIndex(n)).gBest = cell2mat(local_gBest);
                end

            end                    

        case 'pso2cpso'
            for s = 1 : obj.numSwarms
                numExchange = round(obj.generalOptions.exchangeRate*obj.generalOptions.cpso_numIndividuals(s));
                substitutionIndex = round(obj.generalOptions.cpso_numIndividuals(s)/2) + ...
                    randperm(round(obj.generalOptions.cpso_numIndividuals(s)/2), numExchange);

                if s == 1
                    index = 1:obj.numParameters(1);
                else
                    index = 1+sum(obj.numParameters(1:s-1)):sum(obj.numParameters(1:s-1)) + obj.numParameters(s);
                end

                % Apply the exchange
                for n = 1 : numExchange
                    if n==1
                        % Copy the global best of the pso
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).position = obj.pso_swarm.gBest.position(index);
                        %obj.cpso_swarms(s).swarm(substitutionIndex(n)).velocity = zeros(obj.numParameters(s), 1);
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).fitness = obj.pso_swarm.gBest.fitness;
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).pBest= obj.pso_swarm.gBest.position(index);
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).pBestFitness = obj.pso_swarm.gBest.fitness;
                        %obj.cpso_swarms(s).swarm(substitutionIndex(n)).gBest = obj.pso_swarm.gBest.position(index);       
                    else
                        % Select the particles from the PSO swarm
                        % with a K tournament algorithm
                        K = round(obj.generalOptions.kRateExchange*obj.generalOptions.pso_numIndividuals);
                        selectedIndex = sort(randperm(obj.generalOptions.pso_numIndividuals, K));
                        selectedIndex = selectedIndex(1);

                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).position = obj.pso_swarm.swarm(selectedIndex).position(index);
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).velocity = zeros(obj.numParameters(s), 1);
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).fitness = obj.pso_swarm.swarm(selectedIndex).fitness;
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).pBest = obj.pso_swarm.swarm(selectedIndex).pBest(index);
                        obj.cpso_swarms(s).swarm(substitutionIndex(n)).pBestFitness = obj.pso_swarm.swarm(selectedIndex).pBestFitness;
                        %obj.cpso_swarms(s).swarm(substitutionIndex(n)).gBest = obj.pso_swarm.swarm(selectedIndex).gBest(index);  
                    end
                end        
            end            
    end
end