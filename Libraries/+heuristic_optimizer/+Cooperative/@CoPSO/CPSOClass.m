classdef CPSOClass
    properties
        % Swarm
        cpso_swarms;
        numSwarms;
        pso_swarm;
        numParameters;
        
        % Fitness
        Fitness;
        fitnessOptions;
          
        % Global bests
        gBest;
        old_gBest;
        
        % General Options
        generalOptions;
        
        % PSO Options
        PSOOptions;
        
        % GA Options
        GAOptions;
        
        % Plot 
        PlotFunction;
    end
    
    methods
        % Constructor
        function obj = CPSOClass(p_Fitness, p_numParameters, varargin)
            import heuristic_optimizer.PSO.*
            
            % Handle less than required arguments 
            if nargin <3
                error('The constructor must receive at leas two arguments: 1. The handle to the fitness function; 2. the number of swarms; the number of parameters to optimize');
            end
            
            % Default algorithm parameters
            default_generalOptions = struct(...
                'parallelFlag',             false,      ... flag for the parallel evaluation
                'adaptiveFlag',             false,      ... flag for the adaptive velocity update
                'coopMode',                 'series',   ... cooperative mode between the swarms ('series', 'parallel')
                'cpso_numIndividuals',      20*ones(length(p_numParameters),1),    ... num individuals of each cpso
                'pso_numIndividuals',       20,                         ... num individuals of the auxiliary pso
                'initializationMode',       'normal',	... type of initialization (zero, uniform, normal)
                'initializationRange',      [-1, 1],	... Range of the initialization
                'exchangeRate',             1e-1,       ... Rate of the exchange between CPSO and auxiliary PSO
                'kRateExchange',            1e-1,       ... Rate for K-tournament in selecting particles to exchange
                'numBests',                 1,          ... Non used in CPSO
                'killRatio',                1,          ... Non used in CPSO
                'maxDelta',                 0.1,        ... max variation allowed for guaranteed convergence pso
                'maxVelocity',              0.5,        ... max variation allowed for velocity
                'numSubIter',               ones(length(p_numParameters),1),          ... number of sub iteration for each swarms
                'maxIteration',             200,        ... max num of iteration
                'consecutiveTrueTh',        10,         ... threshold for the stop condition
                'stopThreshold',            1e-5,       ... threhsold on the difference between old and new gBest
                'fitnessStopThreshold',     1e-5        ... threshold on the difference between old best fitness and new best fitness
                );
            
            % Default PSO parameters
            default_PSOOptions = struct(      ...
                'w',     0.7298,      ...   Inertial coefficient
                'c_p',   1.49618,     ...   Weight of personal best
                'c_g',   1.49618      ...   Weight of global best
                );

            % Default Genetic Hybridization parameters
            default_GAOptions = struct(         ...
                'kRate',         0.1,	... kRate for K tournament used in GA hybridization
                'mutationRate',  0.1,   ... rate for the number of mutation
                'crossoverRate', 0.1	... rate for the number of crossovers  
                );

            % Define the input parser
            p = inputParser;
            p.addRequired('p_Fitness');
            p.addRequired('p_numParameters');
            p.addParameter('generalOptions', default_generalOptions);
            p.addParameter('PSOOptions', default_PSOOptions);
            p.addParameter('GAOptions', default_GAOptions);
            p.addParameter('fitnessOptions', []);
            p.addParameter('PlotFunction', []);
           
            % Parse arguments
            p.parse(p_Fitness, p_numParameters, varargin{:});
            
            % Fitness Options
            obj.Fitness = p_Fitness;
            obj.fitnessOptions = p.Results.fitnessOptions;
            
            % Particle information
            obj.numSwarms = length(p_numParameters);
            obj.numParameters = p_numParameters;
            
            % General Options
            obj.generalOptions = p.Results.generalOptions;
            
            % PSO Options
            obj.PSOOptions = p.Results.PSOOptions;
            
            % GA Options
            obj.GAOptions = p.Results.GAOptions;
            
            % Plotting Options
            obj.PlotFunction = p.Results.PlotFunction;
            
            % Define swarm
            obj.cpso_swarms = HGPSOClass.empty;
            obj.pso_swarm = HGPSOClass.empty;
                    
            % Define gBest and old_gBest
            obj.gBest = struct('position', [], 'fitness', inf);
            obj.old_gBest = struct('position', [], 'fitness', inf);
        end
    end
end