% Update CPSOSwarm
function obj = UpdateCPSOSwarmsParallel(obj)
    import heuristic_optimizer.PSO.*
    
    if obj.generalOptions.parallelFlag
        % Update the swarm
        local_cpsoSwarms = obj.cpso_swarms;
        local_gBest = obj.gBest;
        parfor s=1:obj.numSwarms
            local_cpsoSwarms(s).fitnessOptions.mode = 'cpso';
            local_cpsoSwarms(s).fitnessOptions.gBest = local_gBest;
            local_cpsoSwarms(s).fitnessOptions.currentSwarm = s;

            % Update swarm
            local_cpsoSwarms(s) = local_cpsoSwarms(s).UpdateSwarm(); 

            % Update gBests
            local_cpsoSwarms(s) = local_cpsoSwarms(s).UpdateGBest();

            % Generate new particles with genetic operators
            if ~isempty(local_cpsoSwarms(s).GAOptions)
                local_cpsoSwarms(s) = local_cpsoSwarms(s).GeneticHybridization();
            end
        end
        obj.cpso_swarms = local_cpsoSwarms;
    else
        % Update the swarm
        for s=1:obj.numSwarms
            obj.cpso_swarms(s).fitnessOptions.mode = 'cpso';
            obj.cpso_swarms(s).fitnessOptions.gBest = obj.gBest;
            obj.cpso_swarms(s).fitnessOptions.currentSwarm = s;

            % Update swarm
            obj.cpso_swarms(s) = obj.cpso_swarms(s).UpdateSwarm(); 

            % Update gBests
            obj.cpso_swarms(s) = obj.cpso_swarms(s).UpdateGBest();

            % Generate new particles with genetic operators
            if ~isempty(obj.cpso_swarms(s).GAOptions)
                obj.cpso_swarms(s) = obj.cpso_swarms(s).GeneticHybridization();
            end
        end
    end
    
    % Update gBest
    temp_gBest = obj.gBest;
    for s=1:obj.numSwarms
        temp_gBest.position{s} = obj.cpso_swarms(s).gBest.position;
    end
    % Evaluate fitness for the gBest particle
    obj.fitnessOptions.mode = 'gBest';
    temp_gBest.fitness = obj.Fitness(temp_gBest.position, obj.fitnessOptions);

    if temp_gBest.fitness < obj.gBest.fitness
        obj.gBest.position = temp_gBest.position;
        obj.gBest.fitness = temp_gBest.fitness;
    end
end