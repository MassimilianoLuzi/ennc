% Update CPSOSwarm
function obj = UpdateGBest(obj)
    import heuristic_optimizer.PSO.*
    
    if obj.gBest.fitness < obj.pso_swarm.gBest.fitness
        obj.pso_swarm.gBest.position = cell2mat(obj.gBest.position);
        obj.pso_swarm.gBest.fitness = obj.gBest.fitness; 
    else
        for s = 1:obj.numSwarms 
            if s == 1
                index = 1:obj.numParameters(1);
            else
                index = 1+sum(obj.numParameters(1:s-1)):sum(obj.numParameters(1:s-1)) + obj.numParameters(s);
            end
            obj.gBest.position{s} = obj.pso_swarm.gBest.position(index);
        end
        obj.gBest.fitness = obj.pso_swarm.gBest.fitness;
    end
end